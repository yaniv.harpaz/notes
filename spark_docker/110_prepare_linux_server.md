# Install linux packages


During this installation process, it is required to create / edit files. The most common and simple way is with **vim**. In order to have things flowing and make the process as automatic as possible, I'm using here 2 technics in order to avoid vim, so it would be faster to use. If you feel more comfortable with using vim, go ahead.

* **"sed -i"** will be changing values in-place, inside a file.
* **"cat > my_file_name.ext << EOF"** will create or run-over the existing file with the content (till reaching EOF)
* **"cat >> my_file_name.ext << EOF"** will add at the end of the file the content (till reaching EOF)


### go superuser
```
sudo su
apt-get update -y
```

### Install RPMs (some are required, some are bonus but very useful)

```
apt-get install -y ncdu mc docker.io python3-pip python-virtualenv

```

### add the ubuntu user (the basic regular login user) to the docker group
```
usermod -aG docker ubuntu 
```

### create the data volume on docker
```
docker volume create data
```

### download data into the data volume
```
cd /var/lib/docker/volumes/data/_data/
wget https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_2017-01.csv
wget https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_2017-02.csv
wget https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_2017-03.csv
wget https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_2017-04.csv
wget https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_2017-05.csv
wget https://s3.amazonaws.com/nyc-tlc/trip+data/green_tripdata_2017-06.csv

```

### Run the spark docker with the data volume attached (with python 2.7 or 3.6)
```
docker container run -dit --name sptest --mount source=data,target=/data radanalyticsio/openshift-spark:latest
or
docker container run -dit --name sptest --mount source=data,target=/data radanalyticsio/openshift-spark-py36:latest

```

### Connect with bash to the container running
```
docker exec -it sptest /bin/bash
```

### if you use python 3
```
export PYSPARK_PYTHON=/opt/rh/rh-python36/root/bin/python
export PYSPARK_DRIVER_PYTHON=/opt/rh/rh-python36/root/bin/python
```

### run the pyspark REPL (python)
```
pyspark
```

### run demo on the data (python)
```
# python demo in Spark, finding the busy hours in cab rides
import socket
from pprint import pprint as pp
from collections import defaultdict
from time import gmtime, strftime

from pyspark import SparkContext, SparkConf

conf = SparkConf().setAppName('MyFirstStandaloneApp')
sc = SparkContext(conf=conf)

# dictionary comprehension for 24 hours 
hours = { str(hour).zfill(2) : 0 for hour in range(24) }

# use current hostname as hostname
namenode = socket.getfqdn()
my_raw_rdd = sc.textFile("/data/green_tripdata_2017-*.csv".format(namenode))

# go with all the data (requires more resources / tuning)
#my_raw_rdd = sc.wholeTextFiles("hdfs://{}/user/admin/trips/".format(socket.getfqdn()))

# break into lines
# break into row items
# filter only timestamp data
# filter only the hours component of the timestamp
my_records_rdd = my_raw_rdd.flatMap(lambda line: line.split('\n')) \
                            .flatMap(lambda line: line.split(',')) \
                            .filter(lambda item: len(item) == 19 ) \
                            .map(lambda item: item.split(' ')[1][:2])       
for hour in my_records_rdd.collect():
    hours[hour] += 1

hours_sorted = list()
hours_sorted = sorted(hours.items(), key=lambda x: (-x[1], x[0]))
pp(hours_sorted)
```

