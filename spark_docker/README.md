# Run Spark on docker (running on EC2 Ubuntu) on Amazon Web Services (AWS) http://bit.ly/dockerspark


These notes are for **demo purpose** only, not for production installation.

##### Based on Ubuntu Server 18.04 LTS 64-bit // t2.xlarge servers (4 cores / 16GB) take it 30GB storage size

This section is intended for people who would like to try and install a Cloudera Hadoop Cluster (with one or more servers) on top of Amazon's EC2 (Elastic Compute Cloud) servers. It is also relevant to anyone who is using any basic installation of Linux (supported by Cloudera). I am using a basic Linux image provided by Amazon. I provided info how to do it by yourself, without automatic scripts. I want this to be as simple as possible, so anyone can use it and understand in each and every step, what I'm trying to do.


### Linux preparations for the server and a simple spark demo (less than 1GB docker)
* [110_prepare_linux_server](https://gitlab.com/yaniv.harpaz/notes/blob/master/spark_docker/110_prepare_linux_server.md)

### Linux preparations for the server and a jupyter spark demo (more than 5GB size docker)
* [120_prepare_linux_server](https://gitlab.com/yaniv.harpaz/notes/blob/master/spark_docker/120_prepare_linux_server.md)




