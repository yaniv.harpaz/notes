drop TABLE trips;

CREATE external TABLE trips (
VendorID                        int
,tpep_pickup_datetime           timestamp
,tpep_dropoff_datetime          timestamp
,passenger_count                int
,trip_distance                  float
,RatecodeID                     int
,store_and_fwd_flag             string
,PULocationID                   int
,DOLocationID                   int
,payment_type                   int
,fare_amount                    float
,extra                          float
,mta_tax                        float
,tip_amount                     float
,tolls_amount                   float
,improvement_surcharge          float
,total_amount                   float
)
partitioned by (trip_month string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
location '/user/admin/trips';


CREATE external TABLE trips (
VendorID                        int
,tpep_pickup_datetime           timestamp
,tpep_dropoff_datetime          timestamp
,passenger_count                int
,trip_distance                  float
,RatecodeID                     int
,store_and_fwd_flag             string
,PULocationID                   int
,DOLocationID                   int
,payment_type                   int
,fare_amount                    float
,extra                          float
,mta_tax                        float
,tip_amount                     float
,tolls_amount                   float
,improvement_surcharge          float
,total_amount                   float
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
location '/user/admin/trips1';




1,2017-01-09 11:13:28,2017-01-09 11:25:45,1,3.30,1
,N,263,161,1,12.5
,0,0.5,2,0,0.3,15.3


hdfs dfs -put yellow_tripdata_2017-05.csv /user/admin/trips/trip_month=2017-05
hdfs dfs -put yellow_tripdata_2017-04.csv /user/admin/trips/trip_month=2017-04
hdfs dfs -put yellow_tripdata_2017-03.csv /user/admin/trips/trip_month=2017-03
hdfs dfs -put yellow_tripdata_2017-02.csv /user/admin/trips/trip_month=2017-02
hdfs dfs -put yellow_tripdata_2017-01.csv /user/admin/trips/trip_month=2017-01
