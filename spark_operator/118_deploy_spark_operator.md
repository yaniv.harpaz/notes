 # Deploy Google's Spark Operator

### helm add repo // google spark operator
```
helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator
```

### helm 3 // install
```
helm install incubator/sparkoperator --generate-name --skip-crds
```

### you will get an output like this - find the NAME 
ubuntu-test01# helm install incubator/sparkoperator --generate-name --skip-crds

**NAME: sparkoperator-1587235900**

LAST DEPLOYED: Sat Apr 18 18:51:41 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None


### with the name you found you can check its status
```
helm status sparkoperator-1587235900
```

### clone google's git repo
```
git clone https://github.com/GoogleCloudPlatform/spark-on-k8s-operator.git
cd spark-on-k8s-operator 
```

### apply the Service account and the role binding
```
kubectl apply -f manifest/spark-rbac.yaml
```

### increase the number of executors in the spark example
```
sed -i 's/instances: 1/instances: 4/' examples/spark-pi.yaml
```

### launch the Spark App
```
kubectl apply -f examples/spark-pi.yaml
```

![](Google-Spark-Operator-Test.gif)
