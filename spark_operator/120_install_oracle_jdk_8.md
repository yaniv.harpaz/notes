# Install the latest Oracle JDK 

### Install Oracle JDK 
```
mkdir -p /usr/java
cd /usr/java
wget --no-check-certificate https://yaniv-public.s3-eu-west-1.amazonaws.com/jdk-8u191-linux-x64.tar.gz

tar xzf jdk-8u191-linux-x64.tar.gz
update-alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_191/bin/java 2
update-alternatives --display java
java -version
rm -f jdk-8u191-linux-x64.tar.gz
```

### Remove openJDK if present (usually it's not present, so you would get "Error: Need to pass a list of pkgs to remove" which is quite alright)
Centos:
```
yum remove -y $(rpm -qa | grep jdk)
```
Ubuntu:
```
apt-get remove -y $(rpm -qa | grep jdk)
```

### Set the JAVA_HOME env variable for all users
```
cat > /etc/profile.d/hadoop_env.sh << EOF
export JAVA_HOME='/usr/java/jdk1.8.0_191'
export SPARK_HOME=/usr/local/spark
EOF
```

## Install scala
```
apt-get install -y scala
```

## Install Spark 2
```
wget https://downloads.apache.org/spark/spark-2.4.5/spark-2.4.5-bin-hadoop2.7.tgz

tar -xvf spark-2.4.5-bin-hadoop2.7.tgz
```

### Move the resulting folder and create a symbolic link so that you can have multiple versions of Spark installed.
```
sudo mv spark-2.4.5-bin-hadoop2.7 /usr/local/
sudo ln -s /usr/local/spark-2.4.5-bin-hadoop2.7/ /usr/local/spark
export SPARK_HOME=/usr/local/spark
export PATH=$SPARK_HOME/bin:$PATH
```

### add this to the bashrc file or /etc/bash.bashrc
```
echo "export SPARK_HOME=/usr/local/spark" >> /etc/bash.bashrc
echo "export PATH=$SPARK_HOME/bin:$PATH" >> /etc/bash.bashrc
```



