# python demo in Spark, finding the busy hours in cab rides
import socket
from pprint import pprint as pp
from collections import defaultdict
from time import gmtime, strftime

from pyspark import SparkContext, SparkConf

conf = SparkConf().setAppName('MyFirstStandaloneApp')
sc = SparkContext(conf=conf)

# dictionary comprehension for 24 hours 
hours = { str(hour).zfill(2) : 0 for hour in range(24) }

# use current hostname as hostname
namenode = socket.getfqdn()
#my_raw_rdd = sc.textFile("hdfs://{}/user/admin/trips/green_tripdata_2017-01.csv".format(namenode))
my_raw_rdd = sc.textFile("/hadoop/user/admin/trips/green_tripdata_2017-01.csv")

# if you want to scan green_tripdata_2017-01 - green_tripdata_2017-06
# uncomment this line:  
#my_raw_rdd = sc.textFile("/hadoop/user/admin/trips/green_tripdata_2017-*.csv")

# go with all the data (requires more resources / tuning)
#my_raw_rdd = sc.wholeTextFiles("hdfs://{}/user/admin/trips/".format(socket.getfqdn()))

# break into lines
# break into row items
# filter only timestamp data
# filter only the hours component of the timestamp
my_records_rdd = my_raw_rdd.flatMap(lambda line: line.split('\n')) \
                            .flatMap(lambda line: line.split(',')) \
                            .filter(lambda item: len(item) == 19 ) \
                            .map(lambda item: item.split(' ')[1][:2])       
for hour in my_records_rdd.collect():
    hours[hour] += 1

hours_sorted = list()
hours_sorted = sorted(hours.items(), key=lambda x: (-x[1], x[0]))
pp(hours_sorted)


current_time = strftime("%Y%m%d%H%M%S", gmtime())

my_records_rdd.take(22)
my_raw_rdd.take(10)

# write to output
#outputDirectory = "hdfs://{}/tmp/YH{}".format(namenode, current_time)
outputDirectory = "/hadoop/user/admin/tmp/{}_{}".format(namenode, current_time)
print(outputDirectory)
my_records_rdd.saveAsTextFile(outputDirectory)

# in order to run it batch on YARN:
# spark2-submit --deploy-mode client --master yarn 270_spark_demo.py
#