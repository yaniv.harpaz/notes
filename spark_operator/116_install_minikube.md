 # Install MiniKube

### Install kubectl - Option 1
```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list

apt-get update

apt-get install -y kubectl
```

### Install kubectl - Option 2
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x ./kubectl
sudo mv ./kubectl /usr/local/sbin/kubectl
```

### Verify kubectl version
```
kubectl version --client -o json 
```


### Install minikube
```
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x minikube-linux-amd64
sudo mv minikube-linux-amd64 /usr/local/bin/minikube
sudo echo "export PATH=$PATH:/usr/local/bin" | sudo tee /etc/profile.d/path_usr_local_bin.sh
```

### Verify minikube version (might need to re-login for path update)
```
minikube version
```

### Start minikube (on a cloud VM it's best not to use a Hypervisor driver)
```
sudo /usr/local/bin/minikube start --vm-driver=none
```

## Install helm
```
wget https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz

tar xvf helm-v3.0.2-linux-amd64.tar.gz

sudo mv linux-amd64/helm /usr/local/bin/

rm helm-v3.0.2-linux-amd64.tar.gz
rm -rf linux-amd64
```

### Verify helm version
```
helm version
```


