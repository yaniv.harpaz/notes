# Launch SQL Server developer edition docker version

# Ubuntu
```
sudo docker pull mcr.microsoft.com/mssql/server:2019-CU3-ubuntu-18.04

sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=StrongPass123@" \
   -p 1433:1433 --name sql1 \
   -v sqldata1:/var/opt/mssql \
   -d mcr.microsoft.com/mssql/server:2019-CU3-ubuntu-18.04
```


# Centos / RHEL
```
sudo docker pull mcr.microsoft.com/mssql/rhel/server:2019-CU1-rhel-8

sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=StrongPass123@" \
   -p 1433:1433 --name sql1 \
   -v sqldata1:/var/opt/mssql \
   -d mcr.microsoft.com/mssql/rhel/server:2019-CU1-rhel-8
```

### access to data files / log files / backup
```
docker inspect volume sqldata1
```

### start a bash shell inside the docker container
```
docker exec -it sql1 /bin/bash
```

### sqlcmd into the DB
```
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "StrongPass123@" 
```

### example of database creation and backup which can be seen on the volume
```
create database docker_demo_db
go

backup DATABASE docker_demo_db to disk='docker_demo_db.bak'
go
```

