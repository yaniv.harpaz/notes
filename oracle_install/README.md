# Install Oracle RDBMS 19c on Azure cloud

These notes are for **demo purpose** only, not for production installation. Tested on Centos 7

##### Based on Centos 7 64-bit // Tested on 4 cores / 16GB RAM


### When you decide to shutdown (for a clean startup when needed)
1. [ not written yet ]
2. [ not written yet ]
3. Only after both are offline, turn off the linux servers (from the AWS Console or with **"shutdown -P now"**)

### Linux preparations for the server and docker installation
* [110_prepare_linux_server](../linux_basic_setup/110_prepare_linux_server.md)

### In case you want to add / manage the disk volumes on the server hosts
* [115_how_to_add_a_volume](../linux_basic_setup/115_how_to_add_a_volume.md)

### Install Oracle RDBMS Software
* [150_oracle_server](150_oracle_server.md)

### Spark 2, JDK installation Version 1.8 and Scala (Optional)
* [120_install_oracle_jdk_8](https://gitlab.com/yaniv.harpaz/notes/blob/master/spark_operator/120_install_oracle_jdk_8.md)

### Install Spark Operator ( radanalyticsio / spark-operator )
* [150_launch_CM_installer](https://gitlab.com/yaniv.harpaz/notes/blob/master/spark_operator/150_launch_CM_installer.md)

### Load data (NYC Transportation example) - Credit to Khen Moscovici for the dataset
* [250_load_nyc_trips](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/250_load_nyc_trips.md)

### Spark Demo (used with the user hdfs, on pyspark)
* [270_spark_demo](https://gitlab.com/yaniv.harpaz/notes/blob/master/spark_operator/270_spark_demo.py)



