# Install linux packages and docker

this note is relevant for all the hosts in the cluster

During this installation process, it is required to create / edit files. The most common and simple way is with **vim**. In order to have things flowing and make the process as automatic as possible, I'm using here 2 technics in order to avoid vim, so it would be faster to use. If you feel more comfortable with using vim, go ahead.

* **"sed -i"** will be changing values in-place, inside a file.
* **"cat > my_file_name.ext << EOF"** will create or run-over the existing file with the content (till reaching EOF)
* **"cat >> my_file_name.ext << EOF"** will add at the end of the file the content (till reaching EOF)


# Centos / Red Hat
```
sudo yum update -y
```

### Install basic RPMs 
```
sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install -y epel-release apt-transport-https conntrack git mc ncdu zsh htop vim gcc 
```

### Install python RPMs 
```
sudo yum install -y python36 python36-devel python36-pip python-devel python-virtualenv
sudo yum install -y bind-utils mlocated yum-utils createrepo bin-utils openssh-clients perl parted
```

### Install Oracle pre-req RPMs 
```
sudo yum install -y binutils.x86_64 compat-libcap1.x86_64 gcc.x86_64 gcc-c++.x86_64 
sudo yum install -y glibc.i686 glibc.x86_64 glibc-devel.i686 glibc-devel.x86_64 ksh compat-libstdc++-33 
sudo yum install -y libaio.i686 libaio.x86_64 libaio-devel.i686 libaio-devel.x86_64 
sudo yum install -y libgcc.i686 libgcc.x86_64 libstdc++.i686 libstdc++.x86_64 libstdc++-devel.i686 
sudo yum install -y libstdc++-devel.x86_64 libXi.i686 libXi.x86_64 
sudo yum install -y libXtst.i686 libXtst.x86_64 make.x86_64 sysstat.x86_64
```

### Install Oracle pre-install RPM
```
cd /tmp
curl -o oracle-database-preinstall-19c-1.0-2.el7.x86_64.rpm https://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64/getPackage/oracle-database-preinstall-19c-1.0-2.el7.x86_64.rpm

sudo yum -y localinstall oracle-database-preinstall-19c-1.0-2.el7.x86_64.rpm
```

### Copy the pre-downloaded Oracle RDBMS RPM file
```
cp /mnt/simplefileshare/install/Oracle/*.rpm /tmp

yum -y localinstall oracle-database-ee-19c-1.0-1.x86_64.rpm
```

# Make sure docker runs under regular non-admin users
### Set the proper environment variables
```
export ORACLE_HOME=/opt/oracle/product/19c/dbhome_1
export ORACLE_SID=testdb

export ORACLE_VERSION=19c
export ORACLE_SID=testdb
export TEMPLATE_NAME=General_Purpose.dbc
export CHARSET=AL32UTF8
export PDB_NAME=testpdb1
export LISTENER_NAME=LISTENER
export NUMBER_OF_PDBS=1
export CREATE_AS_CDB=false

export PATH=$ORACLE_HOME/bin:$PATH
```
### docker service starts on boot
```
sudo systemctl enable docker
```

### Allow using docker without root 
### (might work only after logoff/logon or even restart the machine)
```
sudo groupadd docker
sudo usermod -aG docker $USER
sudo service docker restart
```

### Usualy now you need to login again and then the "docker run" would work
```
docker run hello-world
```

# Optional - Oh My Zsh (each user should be installing it separately)
```
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
```

### Start the zsh  
```
zsh
```

### Switch shell 
```
sudo chsh -s `which zsh`
```

### Install oh my zsh plugins (run for each user)
```
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting

sudo sed -i 's/plugins=(git)/plugins=(git git-flow brew history node npm kubectl zsh-autosuggestions zsh-syntax-highlighting)/' ~/.zshrc
```

### agnoster theme (for usage with poweline fonts only and only if root is using zsh)
```
sudo sed -i 's/robbyrussell/agnoster/' ~/.zshrc
echo 'RPROMPT="[%D{%f/%m/%y} | %D{%L:%M:%S}]"' | tee --append ~/.zshrc
```

### or alanpeabody theme (for standard terminals)
```
sudo sed -i 's/robbyrussell/alanpeabody/' ~/.zshrc
```

### add new plug-ins to all users
```
sudo sed -i 's/plugins=(git)/plugins=(git git-flow brew history node npm kubectl zsh-autosuggestions zsh-syntax-highlighting)/' /root/.zshrc
```

### if you want to automatically start with zsh // this will add a command on your bashrc
```
echo "bash -c zsh" | tee --append ~/.bashrc
```

