General notes from Yaniv Harpaz


Detailed Cloudera Hadoop setup for a demo environment on Amazon,
check out my 

[Cloudera Install Guide](https://gitlab.com/yaniv.harpaz/notes/tree/master/cloudera_install)
or at http://bit.ly/clouderainstall


[docker with spark demo](https://gitlab.com/yaniv.harpaz/notes/tree/master/spark_docker)
or at http://bit.ly/dockerspark


[Spark Operator](https://gitlab.com/yaniv.harpaz/notes/tree/master/spark_operator)
or at http://bit.ly/cloudsparkoperator


