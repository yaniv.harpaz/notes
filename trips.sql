drop TABLE trips_part;

CREATE external TABLE trips_part (
VendorID                        int
,tpep_pickup_datetime           timestamp
,tpep_dropoff_datetime          timestamp
,passenger_count                int
,trip_distance                  float
,RatecodeID                     int
,store_and_fwd_flag             string
,PULocationID                   int
,DOLocationID                   int
,payment_type                   int
,fare_amount                    float
,extra                          float
,mta_tax                        float
,tip_amount                     float
,tolls_amount                   float
,improvement_surcharge          float
,total_amount                   float
)
partitioned by (trip_month string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
location '/user/admin/trips';

alter table trips_part add partition(trip_month="2017-01");
alter table trips_part add partition(trip_month="2017-02");
alter table trips_part add partition(trip_month="2017-03");
alter table trips_part add partition(trip_month="2017-04");
alter table trips_part add partition(trip_month="2017-05");
alter table trips_part add partition(trip_month="2017-06");


CREATE external TABLE trips_parq (
VendorID                        int
,tpep_pickup_datetime           timestamp
,tpep_dropoff_datetime          timestamp
,passenger_count                int
,trip_distance                  float
,RatecodeID                     int
,store_and_fwd_flag             string
,PULocationID                   int
,DOLocationID                   int
,payment_type                   int
,fare_amount                    float
,extra                          float
,mta_tax                        float
,tip_amount                     float
,tolls_amount                   float
,improvement_surcharge          float
,total_amount                   float
)
partitioned by (trip_month string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
stored as parquet
location '/user/admin/trips_par';




drop TABLE trips;

CREATE external TABLE trips (
VendorID                        int
,tpep_pickup_datetime           timestamp
,tpep_dropoff_datetime          timestamp
,passenger_count                int
,trip_distance                  float
,RatecodeID                     int
,store_and_fwd_flag             string
,PULocationID                   int
,DOLocationID                   int
,payment_type                   int
,fare_amount                    float
,extra                          float
,mta_tax                        float
,tip_amount                     float
,tolls_amount                   float
,improvement_surcharge          float
,total_amount                   float
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
location '/user/admin/trips1';



drop TABLE trips_green;

CREATE external TABLE trips_green (
VendorID                        int
,lpep_pickup_datetime           timestamp
,Lpep_dropoff_datetime          timestamp
,Store_and_fwd_flag             string
,RateCodeID                     int
,Pickup_longitude               float
,Pickup_latitude                float
,Dropoff_longitude              float
,Dropoff_latitude               float
,Passenger_count                int
,Trip_distance                  float
,Fare_amount                    float
,Extra                          float
,MTA_tax                        float
,Tip_amount                     float
,Tolls_amount                   float
,Ehail_fee                      float
,improvement_surcharge          float
,Total_amount                   float
,Payment_type                   int
,Trip_type                      int
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
location '/user/admin/trips_green';




hive> 
set hive.exec.dynamic.partition.mode=nonstrict;

insert overwrite table trips_parq partition (trip_month) select * from trips_part;



from pprint import pprint as pp
from collections import defaultdict

hours = defaultdict()
for hour_counter in xrange(24):
    current = hour_counter
    if current < 10:
        my_str = '0' + str(current)
    else:
        my_str = str(current)
    print(hour_counter, my_str)
    hours[my_str] = 0
    
my_raw_rdd = sc.textFile("hdfs://ip-172-31-36-67/user/admin/trips_1m/tripdata_1m.csv")
my_records_rdd = my_raw_rdd.flatMap(lambda line: line.split('\n'))

for line in my_records_rdd.collect():
    items = line.split(',')
    try:
        hours[items[1].split(' ')[1][:2]] += 1
        hours[items[2].split(' ')[1][:2]] += 1
    except s:
        pass
    finally:
        pass
hours_sorted = sorted(hours.items(), key=lambda x: (-x[1], x[0]))        
pp(hours_sorted)


#print(hours)

    
        

        
my_raw_rdd = sc.textFile(r"file:///orig_data/tripdata_1m.csv")
my_records_rdd = my_raw_rdd.flatMap(lambda line: line.split('\n'))
my_records_rdd.count()

        

    
    
    

    
    

sc.textFile(file) \
.flatMap(lambda line: line.split(' ')) \
.distinct()






