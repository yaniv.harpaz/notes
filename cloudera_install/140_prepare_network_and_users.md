# Network configurations for the CDH Cluster


I'd like to provide some background and details about the goal of this process. The Cloudera Manager (a.k.a CM) is the tool that performs all the installations on all the hosts in the cluster. This means that we need to make sure that the hosts have linux installed, with a few adjustments and I recommend having the JDK version you pick. From that point on, the CM will take care of the rest of the cluster setup process.

### It's recommended to set up a linux user with sudo permission (password-less) and do everything with this user. Here I've used **hadoop_aws** as my user for that purpose.

So... here's what we need to accomplish:
1. use password authentication (not only by key) - it's optional, on all host
2. the user which we use for the setup (on all hosts) needs to have password-less sudo
3. we need password-less SSH access from the Cloudera Manager server to all the hosts

* If you want to know exactly which ports you need on the cluster, there is the [Cloudera Ports List](https://www.cloudera.com/documentation/enterprise/latest/topics/cdh_ig_ports_cdh5.html). I recommend during the first time you setup the cluster, to open all ports, just to be sure the focus is on the functional stuff (during the first time).


## Enable password authentication 


### Available on video [ SSH password athentication on YouTube ] (https://youtu.be/HGd0XfgD5r0)

### The following commands will change the "PasswordAuthentication" configuration line to "Yes"
(instead of using vim in order to edit these files)
```
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
sed -i 's/#   PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/ssh_config
```

### Restart the SSH service for the changes to take place
```
service sshd restart
```

## Password-less sudo
```
chmod u+w /etc/sudoers
sed -i 's/# %wheel/%wheel/' /etc/sudoers 
```

## Example of usage with hadoop_aws

### create user
```
useradd hadoop_aws
```

### Set the password for the user
```
passwd hadoop_aws
```

### Add the user to the group of sudoers
```
usermod -a -G wheel hadoop_aws
```

# Configure passwordless SSH access

### Available on video [ SSH without a password on YouTube ] (https://youtu.be/kNthmVvpdPg)

### Switch user to hadoop_aws (from the user you plan to use for Cloudera) **on all hosts**
```
su - hadoop_aws
```

### Generate RSA key (and press enter a few times to use the defaults)
```
ssh-keygen -t rsa
```

### From the **Cloudera Manager** server copy the SSH key to **all the hosts** in the cluster
````
ssh-copy-id -o StrictHostKeyChecking=no -i hadoop_aws@SERVER1
ssh-copy-id -o StrictHostKeyChecking=no -i hadoop_aws@SERVER2
ssh-copy-id -o StrictHostKeyChecking=no -i hadoop_aws@SERVER3
ssh-copy-id -o StrictHostKeyChecking=no -i hadoop_aws@SERVERN
````

### Try to connect to all the servers from the Cloudera Manager server (should connect automatically, without a password request)
```
ssh hadoop_aws@SERVER1
````

# hosts file
## ALL servers should contain /etc/hosts with all the servers names inside
### example - should contain the internal hostname and the IP addresses 
### you can find the fully qualified host name with this command:
```
[root@ip-172-31-58-35 centos]# hostname -f
ip-172-31-58-35.ec2.internal
```

### Add the server entries to the /etc/hosts file
```
cat >> /etc/hosts << EOF
ip-172-31-58-35     172.31.58.35
ip-172-31-51-123    172.31.51.123
ip-172-31-54-7      172.31.54.7
EOF
```

