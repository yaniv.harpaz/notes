#!/usr/bin/python
import boto3
import sys
import argparse
import socket


def get_ip():
    """
    Get the ip of the current host
    :return: ip
    """
    return socket.gethostbyname(socket.gethostname())


def main(running_args):
    # Get my current IP address
    current_ip = get_ip()
    # Connect to AWS EC2 objects
    ec2client = boto3.client('ec2')
    response = ec2client.describe_instances()

    # Check the arguments provided from the command line
    list_attributes = (running_args['list-attributes'] is True)
    private_ip = (running_args['private-ip'] is True)
    private_dns = (running_args['private-dns'] is True)
    public_ip = (running_args['public-ip'] is True)
    public_dns = (running_args['public-dns'] is True)
    exclude_local = (running_args['exclude-local'] is True)
    copy_key = (running_args['copy-key'] is True)
    user_name = running_args['user']

    for reservation in response["Reservations"]:
        # Loop through the EC2 server instances
        for instance in reservation["Instances"]:
            ec2 = boto3.resource('ec2')
            specific_instance = ec2.Instance(instance["InstanceId"])
            # print the report by the arguments provided
            if list_attributes:
                print(dir(specific_instance))
                sys.exit(0)
                break
            elif exclude_local and specific_instance.private_ip_address == current_ip:
                continue
            elif private_ip and not private_dns:
                print("{}".format(specific_instance.private_ip_address))
            elif not private_ip and private_dns:
                print("{}".format(specific_instance.private_dns_name))
            elif private_ip and private_dns:
                print("{} \t {}".format(specific_instance.private_ip_address, specific_instance.private_dns_name))
            elif copy_key:
                print("ssh-copy-id -f {}@{}".format(user_name, specific_instance.private_dns_name))
            elif public_ip:
                print("{}".format(specific_instance.public_ip_address))
            elif public_dns:
                print("{}".format(specific_instance.public_dns_name))
            elif copy_key:
                print("ssh-copy-id -f {}@{}".format(user_name, specific_instance.private_dns_name))


if __name__ == "__main__":
    # Parse the command line arguments
    parser = argparse.ArgumentParser(description='List AWS region servers', epilog='Example: list_servers')
    parser.add_argument('-a', '--list-attributes', action='store_true', default='False',
                        dest='list-attributes', help='list instance attributes')
    parser.add_argument('-i', '--private-ip', action='store_true', default='False',
                        dest='private-ip', help='list private ip addresses')
    parser.add_argument('-d', '--private-dns', action='store_true', default='False',
                        dest='private-dns', help='list private DNS names')
    parser.add_argument('-pi', '--public-ip', action='store_true', default='False',
                        dest='public-ip', help='list public ip addresses')
    parser.add_argument('-pd', '--public-dns', action='store_true', default='False',
                        dest='public-dns', help='list public DNS names')
    parser.add_argument('-x', '--exclude-local', action='store_true', default='False',
                        dest='exclude-local', help='exclude local server from list')
    parser.add_argument('-c', '--copy-key', action='store_true', default='False',
                        dest='copy-key', help='generate ssh-copy-id commands')
    parser.add_argument('-u', '--user', action='store', default=None,
                        dest='user', help='user name for ssh-copy-id')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)

    running_args = vars(parser.parse_args())
    main(running_args)
