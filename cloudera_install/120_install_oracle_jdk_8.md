# Install the latest Oracle JDK 
## Can be skipped and have the CM install it for you

### Install Oracle JDK (relevant for all the hosts in the cluster)
```
mkdir -p /usr/java
cd /usr/java
wget --no-check-certificate https://yaniv-public.s3-eu-west-1.amazonaws.com/jdk-8u191-linux-x64.tar.gz

tar xzf jdk-8u191-linux-x64.tar.gz
alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_191/bin/java 2
update-alternatives --display java
java -version
rm -f jdk-8u191-linux-x64.tar.gz
```

### Remove openJDK if present (usually it's not present, so you would get "Error: Need to pass a list of pkgs to remove" which is quite alright)
```
yum remove -y $(rpm -qa | grep jdk)
```

### Set the JAVA_HOME env variable for all users
```
cat > /etc/profile.d/hadoop_env.sh << EOF
export JAVA_HOME='/usr/java/jdk1.8.0_191'
EOF
```

# Install the JDBC Driver
* do not **yum install** the driver, it installs the OpenJDK which will cause problems for the Cloudera products

```
cd /tmp
wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.44.tar.gz

tar xvfz mysql-connector-java-5.1.44.tar.gz

mkdir -p /usr/share/java
cp -pv /tmp/mysql-connector-java-5.1.44/mysql-connector-java-5.1.44-bin.jar /usr/share/java/mysql-connector-java.jar

ls -ltrh /usr/share/java/
```


