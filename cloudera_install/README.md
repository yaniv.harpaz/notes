# Install Cloudera Cluster (CDH) on Amazon (AWS) and Microsoft Azure

These notes are for **demo purpose** only, not for production installation. Tested on CDH 5.16.2

##### Based on Centos 7 64-bit // Tested on the [AWS Centos 7 image](https://aws.amazon.com/marketplace/pp/B00O7WM7QW) on the t2.xlarge servers (4 cores / 16GB)
or Azure DS3 v2 (4 cores / 14GB)

This section is intended for people who would like to try and install a Cloudera Hadoop Cluster (with one or more servers) on top of Amazon's EC2 (Elastic Compute Cloud) servers. It is also relevant to anyone who is using any basic installation of Linux (supported by Cloudera). I am using a basic Linux image provided by Amazon. I provided info how to do it by yourself, without automatic scripts. I want this to be as simple as possible, so anyone can use it and understand in each and every step, what I'm trying to do.


### When you decide to shutdown (for a clean startup when needed)
1. Stop the CDH Cluster (from the Cloudera Manager web management)
2. Stop the Cloudera manager management services (from the Cloudera Manager web management)
3. Only after both are offline, turn off the linux servers (from the AWS Console or with **"shutdown -P now"**)

### Linux preparations for the servers (relevant for all the hosts in the cluster)
* [110_prepare_linux_server](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/110_prepare_linux_server.md)

### In case you want to add / manage the disk volumes on the server hosts
* [115_how_to_add_a_volume](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/115_how_to_add_a_volume.md)

### JDK and JDBC installation Version 1.8 (relevant for all hosts)
* [120_install_oracle_jdk_8](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/120_install_oracle_jdk_8.md)

### Install MariaDB / MySQL as an external DB (optional) Version 10.0 (relevant for the DB host)
* [130_install_mysql](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/130_install_mysql.md)

### Prepare the users and SSH access (relevant for all hosts)
* [140_prepare_network_and_users](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/140_prepare_network_and_users.md)
* Recorded (some of it) on video as well [here](https://youtu.be/HGd0XfgD5r0) and [here](https://youtu.be/kNthmVvpdPg)

### Install a cluster on off-line servers (without direct internet connection) // Prepare a local Cloudera repository
* [145_prepare_local_repo](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/prepare_local_repo.md)
* This section is relevant only if your cluster servers do not have a direct internet connection

### Launch the Cloudera Manager installer (relevant for the Cloudera Manager host)
* [150_launch_CM_installer](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/150_launch_CM_installer.md)

### Testing and Benchmark
* [230_basic_cluster_testing](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/230_basic_cluster_testing.md)

### Load data (NYC Transportation example) - Credit to Khen Moscovici for the dataset
* [250_load_nyc_trips](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/250_load_nyc_trips.md)

### Spark Demo (used with the user hdfs, on pyspark)
* [270_spark_demo](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/270_spark_demo.py)

### AWS Bonus: List Servers (useful for the hosts file generation)
* [290_aws_python_bonus](https://gitlab.com/yaniv.harpaz/notes/blob/master/cloudera_install/290_aws_python_bonus.md)



