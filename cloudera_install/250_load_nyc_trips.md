# load some data - with the hdfs user

### Mount FUSE for easy file system access

* under root
```
mkdir /hadoop
chown hadoop_aws:hadoop /hadoop
hadoop-fuse-dfs dfs://$(hostname):8020 /hadoop
```

* under hdfs
```
cd /hadoop/user
mkdir admin
chmod 775 admin/
cd admin/
mkdir trips/
chmod 775 trips/
cd trips

for year in {2017..2018}; do
  # Loop over the months
  for month in {01..12}; do
    # Construct the download URL
    url="https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_${year}-${month}.parquet"
    # Use wget to download the file
    wget $url
  done
done

```


### Open hive

```
hive

create database trips;
use trips;

drop TABLE trips_green;

CREATE external TABLE trips_green (
VendorID                        int
,lpep_pickup_datetime           timestamp
,Lpep_dropoff_datetime          timestamp
,Store_and_fwd_flag             string
,RateCodeID                     int
,Pickup_longitude               float
,Pickup_latitude                float
,Dropoff_longitude              float
,Dropoff_latitude               float
,Passenger_count                int
,Trip_distance                  float
,Fare_amount                    float
,Extra                          float
,MTA_tax                        float
,Tip_amount                     float
,Tolls_amount                   float
,Ehail_fee                      float
,improvement_surcharge          float
,Total_amount                   float
,Payment_type                   int
,Trip_type                      int
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
location '/user/admin/trips';


select passenger_count, count(*)
from   trips.trips_green
group by passenger_count 
;


```



