# Install mariaDB 10.0 (mysql)

When this note was written, MariaDB Version 10.2 is the latest version,
but it's not supported by Cloudera (I've reached quite a bit of errors using it)
, so this is why I use Version 10.0

### Install the RPMs
```
cat > /etc/yum.repos.d/mariadb.repo << EOF
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.0/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF

yum clean all
yum repolist

yum install -y MariaDB-server MariaDB-client
```

### Configure the DB Centos service
```
systemctl enable mysql
systemctl start mysql
```

### Set the root user of the DB password (skip all the rest question with ENTER)
```
mysql_secure_installation
```

### Create the MySQL databases and users
```
mysql -u root -p 
```

## Inside the MySQL client

The databases are created by default in /var/lib/mysql (each DB in a separate directory).
It is possible to mount /var/lib/mysql on a separate disk if you would like to manage it's size.

```
create database hue DEFAULT CHARACTER SET utf8;
grant all on hue.* TO 'hue'@'%' IDENTIFIED BY 'hue';

create database amon DEFAULT CHARACTER SET utf8;
grant all on amon.* TO 'amon'@'%' IDENTIFIED BY 'amon';

create database scm DEFAULT CHARACTER SET utf8;
grant all on scm.* TO 'scm'@'%' IDENTIFIED BY 'scm';

create database smon DEFAULT CHARACTER SET utf8;
grant all on smon.* TO 'smon'@'%' IDENTIFIED BY 'smon';

create database rman DEFAULT CHARACTER SET utf8;
grant all on rman.* TO 'rman'@'%' IDENTIFIED BY 'rman';

create database hmon DEFAULT CHARACTER SET utf8;
grant all on hmon.* TO 'hmon'@'%' IDENTIFIED BY 'hmon';

create database hive DEFAULT CHARACTER SET utf8;
grant all on hive.* TO 'hive'@'%' IDENTIFIED BY 'hive';

create database nav DEFAULT CHARACTER SET utf8;
grant all on nav.* TO 'nav'@'%' IDENTIFIED BY 'nav';

create database oozie DEFAULT CHARACTER SET utf8;
grant all on oozie.* TO 'oozie'@'%' IDENTIFIED BY 'oozie';
show databases;

flush tables;
flush privileges;

use scm;
flush tables;

exit
```

### Use these settings for mysql
```
cp /etc/my.cnf /etc/orig-my.cnf 

cat > /etc/my.cnf << EOF

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d

[mysqld]
transaction-isolation = READ-COMMITTED
# Disabling symbolic-links is recommended to prevent assorted security risks;
# to do so, uncomment this line:
# symbolic-links = 0

key_buffer = 16M
key_buffer_size = 32M
max_allowed_packet = 32M
thread_stack = 256K
thread_cache_size = 64
query_cache_limit = 8M
query_cache_size = 64M
query_cache_type = 1

max_connections = 550
#expire_logs_days = 10
#max_binlog_size = 100M

#log_bin should be on a disk with enough free space. Replace '/var/lib/mysql/mysql_binary_log' with an appropriate path for your system
#and chown the specified folder to the mysql user.
log_bin=/var/lib/mysql/mysql_binary_log

binlog_format = mixed

read_buffer_size = 2M
read_rnd_buffer_size = 16M
sort_buffer_size = 8M
join_buffer_size = 8M

# InnoDB settings
innodb_file_per_table = 1
innodb_flush_log_at_trx_commit  = 2
innodb_log_buffer_size = 64M
innodb_buffer_pool_size = 4G
innodb_thread_concurrency = 8
innodb_flush_method = O_DIRECT
#innodb_log_file_size = 512M

[mysqld_safe]
#log-error=/var/log/mariadb/mariadb.log
pid-file=/var/run/mariadb/mariadb.pid

EOF
```

### restart service again
```
service mysql restart
service mysql status
```

