# Basic Cluster testing

### Cleanup
```
#!/bin/sh
hdfs dfs -rm -r -skipTrash /tmp/benchmarks/*
hdfs dfs -expunge
```

### PI Calculation
```
time hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-mapreduce-examples.jar pi 10 10000
```

### Teragen & Terasort
```
time hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-0.20-mapreduce/hadoop-examples.jar teragen -D mapreduce.job.maps=8 -D mapreduce.job.reduces=8 100000000 /tmp/benchmarks/t10g_gen
time hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-0.20-mapreduce/hadoop-examples.jar terasort -D mapreduce.job.maps=8 -D mapreduce.job.reduces=8 /tmp/benchmarks/t10g_gen /tmp/benchmarks/t10g_sort
time hadoop jar /opt/cloudera/parcels/CDH/lib/hadoop-0.20-mapreduce/hadoop-examples.jar teravalidate -D mapreduce.job.maps=8 -D mapreduce.job.reduces=8 /tmp/benchmarks/t10g_sort /tmp/benchmarks/t10g_validate
```
