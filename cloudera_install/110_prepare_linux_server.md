# Install linux packages

this note is relevant for all the hosts in the cluster

During this installation process, it is required to create / edit files. The most common and simple way is with **vim**. In order to have things flowing and make the process as automatic as possible, I'm using here 2 technics in order to avoid vim, so it would be faster to use. If you feel more comfortable with using vim, go ahead.

* **"sed -i"** will be changing values in-place, inside a file.
* **"cat > my_file_name.ext << EOF"** will create or run-over the existing file with the content (till reaching EOF)
* **"cat >> my_file_name.ext << EOF"** will add at the end of the file the content (till reaching EOF)


### go superuser
```
sudo su
yum update -y
```

## Install RPMs

* Install this if the epel-release doesn't work

```
yum install –y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
```

### Install RPMs (some are required, some are bonus but very useful)

```
yum install -y epel-release
yum install -y python36 python36-devel python36-pip
yum install -y wget httpd ntp htop mc ncdu vim python-pip python-devel python-virtualenv
yum install -y bind-utils gcc git mlocated
yum install -y yum-utils createrepo bin-utils
yum install -y openssh-clients perl parted
yum groupinstall -y development

```

### Change the line SELINUX=enforcing to SELINUX=permissive on this file
```
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
cat /etc/selinux/config
```

### REBOOT now and check with getenforce (should be disabled or permissive)
```
getenforce
```

### Configure swappiness and hugepages (till the next reboot)
```
sysctl -w vm.swappiness=1
echo never > /sys/kernel/mm/transparent_hugepage/enabled
echo never > /sys/kernel/mm/transparent_hugepage/defrag
```

### Configure swappiness and hugepages permanently
### Edit /etc/sysctl.conf as root
```
cat >> /etc/sysctl.conf << EOF
vm.swappiness = 1
EOF

cat /etc/sysctl.conf
```

### Check the status of the hugepages
```
cat /sys/kernel/mm/transparent_hugepage/*
```

### **Good** output (without hugepages)
```
always madvise [never]
always madvise [never]
```

### **Bad** output (without hugepages)
```
[always] madvise never
[always] madvise never
```


### Create script for hugepages disable
```
cat >> /usr/local/sbin/disable_hugepages.sh << EOF
#!/usr/bin/sh
/bin/echo never > /sys/kernel/mm/transparent_hugepage/enabled
/bin/echo never > /sys/kernel/mm/transparent_hugepage/defrag
EOF
```
### Create script for hugepages enable (rollback)

```
cat >> /usr/local/sbin/enable_hugepages.sh << EOF
#!/usr/bin/sh
/bin/echo always > /sys/kernel/mm/transparent_hugepage/enabled
/bin/echo always > /sys/kernel/mm/transparent_hugepage/defrag
EOF
```

### Set execute permissions

```
chmod +x /usr/local/sbin/disable_hugepages.sh
chmod +x /usr/local/sbin/enable_hugepages.sh
```


### Create Centos service for the hugepages disable script
```
cat >> /etc/systemd/system/disable_hugepages.service << EOF
[Unit]
Description=Description for sample script goes here
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/sbin/disable_hugepages.sh
TimeoutStartSec=0

[Install]
WantedBy=default.target

EOF
```

### Configure and launch the hugepages disable service 
```
systemctl daemon-reload
systemctl enable disable_hugepages.service
systemctl start disable_hugepages.service
```


##### When using vim, I like to disable auto-comment
```
:set formatoptions-=cro
```
