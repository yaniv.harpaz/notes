# Launch the Cloudera Manager

## Option 1 - Use the Cloudera Manager installer bin file

### install the cloudera manager bin 
```
wget http://archive.cloudera.com/cm5/installer/latest/cloudera-manager-installer.bin
chmod u+x cloudera-manager-installer.bin
```

### silent install
```
./cloudera-manager-installer.bin --i-agree-to-all-licenses \
    --noprompt --noreadme --nooptions 
```    
    

## Option 2 - Install the Cloudera manager RPM files

### Configure Cloudera yum repo
```
cd /etc/yum.repos.d/
wget https://archive.cloudera.com/cdh5/redhat/7/x86_64/cdh/cloudera-cdh5.repo
wget https://archive.cloudera.com/cm5/redhat/7/x86_64/cm/cloudera-manager.repo
sudo yum clean all
yum repolist

yum install -y yum cloudera-manager-daemons cloudera-manager-agent cloudera-manager-server
```

### Edit the db.properties file only if you installed an External DB
```
cp /etc/cloudera-scm-server/db.properties /etc/cloudera-scm-server/orig-db.properties

cat > /etc/cloudera-scm-server/db.properties << EOF
# Copyright (c) 2012 Cloudera, Inc. All rights reserved.
#
# This file describes the database connection.
#

# The database type
# Currently 'mysql', 'postgresql' and 'oracle' are valid databases.
com.cloudera.cmf.db.type=mysql

# The database host
# If a non standard port is needed, use 'hostname:port'
com.cloudera.cmf.db.host=localhost

# The database name
com.cloudera.cmf.db.name=scm

# The database user
com.cloudera.cmf.db.user=scm

# The database user's password
com.cloudera.cmf.db.password=scm

# The db setup type
# By default, it is set to INIT
# If scm-server uses Embedded DB then it is set to EMBEDDED
# If scm-server uses External DB then it is set to EXTERNAL
com.cloudera.cmf.db.setupType=EXTERNAL

EOF

cat /etc/cloudera-scm-server/db.properties
```

### Start the Cloudera Manager service
```
service cloudera-scm-server start
```

### You can take a look at this Cloudera manager server logs here
```
cd /var/log/cloudera-scm-server
```


## now you can open your browser and go to the public IP / host name address with the port 7180
## for example: 
## http://54.147.64.78:7180/ 


## Additional parcels:
add this URL for Spark 2.x on your cluster
http://archive.cloudera.com/spark2/parcels/2.4.0.cloudera2/
or
https://archive.cloudera.com/spark2/parcels/latest/

and the CSD 
https://www.cloudera.com/documentation/enterprise/latest/topics/cm_mc_addon_services.html#concept_qbv_3jk_bn__section_xvc_yqj_bn

https://docs.cloudera.com/documentation/spark2/2-4-x/topics/spark2_installing.html

https://www.cloudera.com/documentation/spark2/latest/topics/spark2_packaging.html#versions

copy this 
http://archive.cloudera.com/spark2/csd/SPARK2_ON_YARN-2.4.0.cloudera2.jar

in here:
/opt/cloudera/csd/

add this URL for Anaconda on your cluster
https://repo.continuum.io/pkgs/misc/parcels/
