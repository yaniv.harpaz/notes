# List Servers // Python script

## Bonus: a Python script which helps get the list of servers in your region

### Prerequisites

```
yum install -y wget
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install awscli --upgrade --user
```

### and follow the instructions how to "aws configure"
http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html

### [list_servers.py](list_servers.py)
